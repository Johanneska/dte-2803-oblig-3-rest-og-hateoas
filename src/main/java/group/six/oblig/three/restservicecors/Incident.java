package group.six.oblig.three.restservicecors;

import java.util.Date;

public class Incident {
    private final long id;
    private String tile;
    private String description;
    private String category;
    private int status;
    private String user;
    private final Date timestamp;

    public Incident(long id, String tile, String description, String category, int status, String user, Date timestamp) {
        this.id = id;
        this.tile = tile;
        this.description = description;
        this.category = category;
        this.status = status;
        this.user = user;
        this.timestamp = timestamp;
    }

    public long getId() {
        return id;
    }

    public String getTile() {
        return tile;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public int getStatus() {
        return status;
    }

    public String getUser() {
        return user;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTile(String tile) {
        this.tile = tile;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
