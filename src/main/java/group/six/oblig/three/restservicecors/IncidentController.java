package group.six.oblig.three.restservicecors;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
public class IncidentController {
    Incident testIncident = new Incident(1, "Test", "Dette er en test", "Test", 1, "Nikolai", new Date());
    Incident[] incidents = {testIncident};

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/incident")
    public Incident incident(@RequestParam(required = true) int id){
        if (null == incidents[id-1]){
            return null;
        }
        return incidents[id-1];
    }
}
